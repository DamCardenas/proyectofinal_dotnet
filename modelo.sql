drop database if exists modelo;
create database modelo;
use modelo;

-- modelos
create table autor
(
	id_autor int primary key auto_increment,
    nombre_autor varchar(100),
    nacimiento_autor date,
    muerte_autor date
);

create table libro
(
	id_libro int primary key auto_increment,
    id_autor int,
    titulo_libro varchar(255) unique,
    editorial_libro varchar(255),
    fecha_edicion_libro date,
    precio_libro float,
    stock int,
    foreign key(id_autor) references autor(id_autor)
);


create table cliente
(
	id_cliente int primary key auto_increment,
    nombre_cliente varchar(255),
    nacimiento_cliente date,
    direccion_cliente varchar(255),
    ciudad_cliente varchar(255),
    estado_cliente varchar(255),
    cp_cliente varchar(7)
);


create table empleado
(
	id_empleado int primary key auto_increment,
    nombre_empleado varchar(255),
    nacimiento_empleado date,
    direccion_empleado varchar(255),
    ciudad_empleado varchar(255),
    estado_empleado varchar(255),
    cp_empleado varchar(7),
    ingreso_empleado date
);


/*
Tabla forma de cobro:
Donde se almacenan las formas de cobro 
Ejemplos:
-Tarjeta de credito
-Tarjeta de debito
-Efectivo
-Transferencia electronica
*/ 
create table forma_cobro
(
	id_fcobro int primary key auto_increment,
    nombre_fcobro varchar(255) unique
);


create table pedido
(
	id_pedido int primary key auto_increment,
    fecha_pedido date,
    id_cliente int,
    id_empleado int,
    id_fcobro int,
    foreign key(id_cliente) references cliente(id_cliente),
    foreign key(id_empleado) references empleado(id_empleado),
    foreign key(id_fcobro) references forma_cobro(id_fcobro)
);

/*
estado_pedido:
En esta tabla se genera un historial 
de las acciones llevadas a cabo sobre un pedido
tales como:
- recoleccion de libros
- empaquetado para envio
- recoleccion de pedido
- pedido entregado
- pedido cancelado
- etc...
*/
create table estado_pedido
(
	id_estado int primary key auto_increment,
    id_pedido int,
    fecha_estado datetime,
    titulo_estado varchar(50),
    descripcion_estado text,
    foreign key (id_pedido) references pedido(id_pedido)
);

/*
r_pedido_libro:
Esta tabla nos permite relacionar un pedido con uno o mas libros
y establecer diferentes unidades de cada uno.
*/
create table r_pedido_libro
(
	id_rpl int primary key auto_increment ,
    id_libro int,
    id_pedido int,
    cantidad_rpl int,
    foreign key (id_libro) references libro(id_libro),
    foreign key (id_pedido) references pedido(id_pedido)
);


delimiter //

-- procedures
/*
Ganancias:
retorna el total de ganancias obtenidas 
*/
create procedure ganancias (out output float)
begin
	select (a.cantidad_rpl*b.precio_libro)
    into output
	from r_pedido_libro a
    left join libro b on b.id_libro =a.id_libro;
end //

-- triggers
-- este trigger nos protege de sobre vender libros
create trigger check_r_pedido_libro before insert on r_pedido_libro for each row 
begin
	-- verificar que el libro exista
    if(select a.stock from librp a where a.id_libro=new.id_libro) is not null
    then 
		signal sqlstate '45000' set message_text = 'Error, No se puede agregar un pedido de un libro inexistente';
    end if;
	-- verifica que el stock no sea menor que el del pedido
    if (select a.stock  from libro a where a.id_libro=new.id_libro) < new.cantidad_rpl
    then 
		signal sqlstate '45000' set message_text = 'Error, la cantidad de libros solicitada supera la cantidad de stock';
	else 
		update libro a set a.stock= (a.stock-new.cantidad_rpl) where a.id_libro=new.id_libro; 
    end if;
end//

delimiter ;





